#include "DistrhoPlugin.hpp"
#include "sampler.h"

START_NAMESPACE_DISTRHO

class samplersaurus : public Plugin
{

	public:

	sampler* sampler1;

	samplersaurus() : Plugin(kParameterCount, 0, 0), gain(1.0) {}
                       
        // sampler1.zones.push_back(zone);
        
	void initParameter (uint32_t index, Parameter& parameter) override
	{
		switch (index)
		{
			case frog1:
				parameter.name = "frog1";
				parameter.symbol = "frog1";
				parameter.ranges.def = 1.0f;
				parameter.ranges.min = 0.0f;
				parameter.ranges.max = 2.0f;
				break;
				
			case frog2:
				parameter.name = "frog1";
				parameter.symbol = "frog1";
				parameter.ranges.def = 1.0f;
				parameter.ranges.min = 0.0f;
				parameter.ranges.max = 2.0f;
				break;
		        
			default:
				break;
		}
	}
	
	

    private:
    
        
    protected:

        const char *getLabel() const override { return "samplersaurus"; }
        
        const char *getDescription() const override
        {
            return "Sampler Synthesizer Plugin";
        }
        
        const char *getMaker() const override { return "thunderox"; }
        
        const char *getLicense() const override { return "MIT"; }
        
        uint32_t getVersion() const override { return d_version(0,0,1); }
        
        int64_t getUniqueId() const override
        { 
            return d_cconst('T','O','S','S'); 
        }
        
	float gain;
                
	//--------------------------------------------------------------------------
        
	float getParameterValue(uint32_t index) const override
	{
		switch (index)
		{
			case frog1:
		    		return gain;
		    		
		    	case frog2:
		    		return gain;
		    		
			default:
		    		return 0.0;
		}
	}
	
	//--------------------------------------------------------------------------

	void setParameterValue(uint32_t index, float value) override
	{
		switch (index) 
		{
			case frog1:
				gain = value;
				break;
			
			case frog2:
				gain = value;
				break;
			
			default:
				break;
		}
	}
	
	//--------------------------------------------------------------------------
        
	void run(const float** inputs, float** outputs, uint32_t frames,
		const MidiEvent* midiEvents, uint32_t midiEventCount) override
		
	{
		float *const out = outputs[0];
		float* out_left = outputs[0];
		float* out_right = outputs[1];
		

		memset( out_left, 0, sizeof(double)*(frames*0.5) );
		memset( out_right, 0, sizeof(double)*(frames*0.5) );

		for (uint32_t i = 0; i < frames; i++)
		{
			out[i] = out[i];
		}
	}

        DISTRHO_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(samplersaurus);
};

//--------------------------------------------------------------------------

Plugin* createPlugin()
{
	samplersaurus* new_samplersaurus = new samplersaurus();
	new_samplersaurus->sampler1 = new sampler();
	new_samplersaurus->sampler1->load_instrument("/home/odin/programming/audio/gitlab/work/sfz/Chorus - Male.sfz");
	
	return new_samplersaurus;
}

END_NAMESPACE_DISTRHO







