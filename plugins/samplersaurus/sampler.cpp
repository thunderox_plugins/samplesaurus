
#include "sampler.h"


//--------------------------------------------------------------------------------------

sampler::sampler()
{
}

//--------------------------------------------------------------------------------------
sampler::~sampler()
{

}


//--------------------------------------------------------------------------------------
bool sampler::load_instrument(string file_name)
{
	FILE* file = fopen( file_name.c_str() , "r");
	
	if (file == NULL)
	{
		cout << "Instrument not found: " << file_name << endl;
		return false;
	}
	else
	{
		cout << "Instrument found: " << file_name << endl;
	}

	return true;
}

