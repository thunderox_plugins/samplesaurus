
#include <vector>
#include <iostream>
#include <string>
#include <samplerate.h>
#include <sndfile.h>

using namespace std;

#define ENV_STATE_ATTACK 1
#define ENV_STATE_DECAY 2
#define ENV_STATE_SUSTAIN 3
#define ENV_STATE_RELEASE 4
#define ENV_STATE_WAIT 5
#define ENV_STATE_DORMANT 6
#define ENV_STATE_KILL 7

//-----------------------------------------------------------------

typedef struct
{
	string name;
	float* buffer;
	double length;
	double tuning;
	float volume;
} zone;

//-----------------------------------------------------------------

class voice
{
	int note;
	float volume;
	float frequency;
	int current_zone;
	SRC_STATE *src_resample;
	SRC_DATA mySampleData;
	int error_sc;
};

//-----------------------------------------------------------------

class sampler
{
	public:
	
	sampler();
	~sampler();
	bool load_instrument(string);

	string name;	
	string file_name;
	vector <zone> zones;
	vector <voice> voices;	
};








